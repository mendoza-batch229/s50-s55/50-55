import {Card, Button} from 'react-bootstrap'
import {useState} from 'react'
import {Link} from 'react-router-dom'

export default function CourseCard({courseProps}){
	// Checks to see if the data was successfully passed
	console.log(courseProps);
	console.log(typeof courseProps);

	// Destructuring the data to avoid dot notation
	const {name, description, price, _id} = courseProps;

	// 3 Hooks in React
	// 1. useState
	// 2. useEffect
	// 3. useContext

	// Use the useStatae hook for the component to be able to store state
	// States are used to keep track of information related to individual components
	// Syntax -> const [getter, setter] = useState(initialGetterValue);

	const [count, setCount] = useState(0);
	const [seats, setSeat] = useState(10);
	console.log(useState(0));

/*	function enroll(){

		if (seats > 0){
			setCount(count + 1);
			console.log("Enrollees " + count);
			setSeat(seats - 1)
			console.log("Seats " + count);
		}else{
			alert("No more seats available.");
		}

	}*/


	return(
		 <Card className="my-3">
	        <Card.Body>
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            <Card.Text>Enrollees: {count}</Card.Text>
	            <Card.Text>Slots Avilable: {seats}</Card.Text>
	            <Link className="btn btn-primary" to={`/courseView/${_id}`}>Details</Link>
	        </Card.Body>
	    </Card>
		)
}